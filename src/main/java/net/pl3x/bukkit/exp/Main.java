package net.pl3x.bukkit.exp;

import net.pl3x.bukkit.exp.commands.Cash2Exp;
import net.pl3x.bukkit.exp.commands.Exp2Cash;
import net.pl3x.bukkit.exp.commands.ExpBalance;
import net.pl3x.bukkit.exp.commands.Pl3xExp;
import net.pl3x.bukkit.exp.configuration.Lang;
import net.pl3x.bukkit.exp.hook.Vault;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin {
    public void onEnable() {
        saveDefaultConfig();

        Lang.reload();

        if (!Vault.setupEconomy()) {
            Logger.info("&4Disabled due to no Vault dependency found!");
            getServer().getPluginManager().disablePlugin(this);
            return;
        }

        getCommand("cash2exp").setExecutor(new Cash2Exp());
        getCommand("exp2cash").setExecutor(new Exp2Cash());
        getCommand("expbalance").setExecutor(new ExpBalance());
        getCommand("pl3xexp").setExecutor(new Pl3xExp(this));

        Logger.info(getName() + " v" + getDescription().getVersion() + " enabled!");
    }

    public void onDisable() {
        Logger.info(getName() + " disabled!");
    }
}
