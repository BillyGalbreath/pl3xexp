package net.pl3x.bukkit.exp.commands;

import net.pl3x.bukkit.exp.ExperienceManager;
import net.pl3x.bukkit.exp.configuration.Lang;
import net.pl3x.bukkit.exp.manager.ChatManager;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class ExpBalance implements CommandExecutor {
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!(sender instanceof Player)) {
            ChatManager.sendMessage(sender, Lang.PLAYER_COMMAND);
            return true;
        }

        if (!sender.hasPermission("command.expbalance")) {
            ChatManager.sendMessage(sender, Lang.COMMAND_NO_PERMISSION);
            return true;
        }

        ExperienceManager expMan = new ExperienceManager((Player) sender);
        Integer exp = expMan.getCurrentExp();

        ChatManager.sendMessage(sender, Lang.EXPBALANCE.replace("{exp}", exp.toString()));
        return true;
    }
}
