package net.pl3x.bukkit.exp.commands;

import net.pl3x.bukkit.exp.Main;
import net.pl3x.bukkit.exp.configuration.Config;
import net.pl3x.bukkit.exp.configuration.Lang;
import net.pl3x.bukkit.exp.manager.ChatManager;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class Pl3xExp implements CommandExecutor {
    private final Main plugin;

    public Pl3xExp(Main plugin) {
        this.plugin = plugin;
    }

    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!sender.hasPermission("command.exp")) {
            ChatManager.sendMessage(sender, Lang.COMMAND_NO_PERMISSION);
            return true;
        }

        if (args.length > 0 && args[0].equalsIgnoreCase("reload")) {
            Config.reload();
            Lang.reload(true);
            ChatManager.sendMessage(sender, Lang.RELOAD.replace("{plugin}", plugin.getName()).replace("{version}", plugin.getDescription().getVersion()));
        }

        ChatManager.sendMessage(sender, Lang.VERSION.replace("{plugin}", plugin.getName()).replace("{version}", plugin.getDescription().getVersion()));
        return true;
    }
}
