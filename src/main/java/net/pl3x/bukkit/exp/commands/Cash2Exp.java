package net.pl3x.bukkit.exp.commands;

import net.milkbowl.vault.economy.EconomyResponse;
import net.pl3x.bukkit.exp.ExperienceManager;
import net.pl3x.bukkit.exp.configuration.Config;
import net.pl3x.bukkit.exp.configuration.Lang;
import net.pl3x.bukkit.exp.hook.Vault;
import net.pl3x.bukkit.exp.manager.ChatManager;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Cash2Exp implements CommandExecutor {
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!(sender instanceof Player)) {
            ChatManager.sendMessage(sender, Lang.PLAYER_COMMAND);
            return true;
        }

        if (!sender.hasPermission("command.cash2exp")) {
            ChatManager.sendMessage(sender, Lang.COMMAND_NO_PERMISSION);
            return true;
        }

        if (args.length < 1) {
            ChatManager.sendMessage(sender, cmd.getDescription());
            return false;
        }

        Player player = (Player) sender;
        ExperienceManager expMan = new ExperienceManager(player);
        double cash;

        try {
            cash = Double.valueOf(args[0]);
        } catch (NumberFormatException e) {
            ChatManager.sendMessage(sender, Lang.NOT_VALID_NUMBER);
            return true;
        }

        if (cash <= 0) {
            ChatManager.sendMessage(sender, Lang.NUMBER_NOT_POSITIVE);
            return true;
        }

        if (cash > Vault.getBalance(player.getUniqueId())) {
            ChatManager.sendMessage(sender, Lang.NOT_ENOUGH_CASH);
            return true;
        }

        EconomyResponse response = Vault.withdrawPlayer(player.getUniqueId(), cash);
        if (!response.transactionSuccess()) {
            ChatManager.sendMessage(sender, Lang.ECONOMY_FAULT.replace("{error}", response.errorMessage));
            return true;
        }

        Integer exp = (int) Math.floor(cash / Config.EXP_COST.getDouble());

        expMan.changeExp(exp);

        ChatManager.sendMessage(sender, Lang.CASH2EXP.replace("{exp}", exp.toString()).replace("{cash}", Vault.format(cash)));
        return true;
    }
}
