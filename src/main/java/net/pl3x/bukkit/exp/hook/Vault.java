package net.pl3x.bukkit.exp.hook;

import net.milkbowl.vault.economy.Economy;
import net.milkbowl.vault.economy.EconomyResponse;
import org.bukkit.Bukkit;
import org.bukkit.plugin.RegisteredServiceProvider;

import java.util.UUID;

public class Vault {
    private static Economy economy = null;

    public static boolean setupEconomy() {
        RegisteredServiceProvider<Economy> economyProvider = Bukkit.getServer().getServicesManager().getRegistration(net.milkbowl.vault.economy.Economy.class);
        if (economyProvider != null) {
            economy = economyProvider.getProvider();
        }
        return (economy != null);
    }

    public static double getBalance(UUID uuid) {
        return economy.getBalance(Bukkit.getOfflinePlayer(uuid));
    }

    public static String format(double amount) {
        return economy.format(amount);
    }

    public static EconomyResponse withdrawPlayer(UUID uuid, double amount) {
        return economy.withdrawPlayer(Bukkit.getOfflinePlayer(uuid), amount);
    }

    public static EconomyResponse depositPlayer(UUID uuid, double amount) {
        return economy.depositPlayer(Bukkit.getOfflinePlayer(uuid), amount);
    }
}
