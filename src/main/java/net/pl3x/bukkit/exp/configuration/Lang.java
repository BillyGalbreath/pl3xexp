package net.pl3x.bukkit.exp.configuration;

import net.pl3x.bukkit.exp.Logger;
import net.pl3x.bukkit.exp.Main;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;

public enum Lang {
    COMMAND_NO_PERMISSION("&4You do not have permission for that command!"),
    PLAYER_COMMAND("&4This command is only available to players."),
    ECONOMY_FAULT("&4An error has occurred: &7{error}"),
    NOT_VALID_NUMBER("&4Not a valid number!"),
    NUMBER_NOT_POSITIVE("&4The value must be a positive number!"),
    NOT_ENOUGH_EXP("&4You do not have enough exp to do that!"),
    NOT_ENOUGH_CASH("&4You do not have enough cash to do that!"),

    CASH2EXP("&dYou have bought &7{exp} &dexp for &7{cash}&d."),
    EXP2CASH("&dYou have sold &7{exp} &dexp for &7{cash}&d."),
    EXPBALANCE("&dYou currently have &7{exp} &dtotal exp."),

    RELOAD("&d{plugin} v{version} reloaded."),
    VERSION("&d{plugin} v{version}.");

    private final String def;

    private static File configFile;
    private static FileConfiguration config;

    Lang(String def) {
        this.def = def;
        reload();
    }

    public static void reload() {
        reload(false);
    }

    public static void reload(boolean force) {
        if (configFile == null || force) {
            String lang = Config.LANGUAGE_FILE.getString();
            Logger.debug("Loading language file: " + lang);
            configFile = new File(Main.getPlugin(Main.class).getDataFolder(), lang);
            if (!configFile.exists()) {
                Main.getPlugin(Main.class).saveResource(Config.LANGUAGE_FILE.getString(), false);
            }
        }
        config = YamlConfiguration.loadConfiguration(configFile);
    }

    private String getKey() {
        return name().toLowerCase().replace("_", "-");
    }

    @Override
    public String toString() {
        String value = config.getString(name());
        if (value == null) {
            value = config.getString(getKey());
        }
        if (value == null) {
            Logger.warn("Missing lang data in file: " + getKey());
            value = def;
        }
        if (value == null) {
            Logger.error("Missing default lang data: " + getKey());
            value = "&c[missing lang data]";
        }
        return ChatColor.translateAlternateColorCodes('&', value);
    }

    public String replace(String find, String replace) {
        return toString().replace(find, replace);
    }
}
